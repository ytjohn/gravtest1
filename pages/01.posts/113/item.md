---
ID: 419
title: "113"
author: ytjohn
date: 03/12/2004
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2004/113
published: true
---
Time plods ahead steadily, as I drift backwards, a stick in the current of raging rapids.
