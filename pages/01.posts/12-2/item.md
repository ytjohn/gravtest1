---
ID: 194
title: "12"
author: ytjohn
date: 02/27/2002
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2002/12-2
published: true
---
I have recently joined in with a new breed of RPG.  Basically starting the "fire-and-forget" class, Progressquest (<a href="http://www.progressquest.com/">www.progressquest.com</a>) will advance with our without you.  Their site is a bit hammered right now from massive traffic, so I have mirrored the download here.  Here is the <a href="/files/games/pq/pq.html">introduction</a>, and the <a href="/files/games/pq/pq.zip">download</a>.
