---
ID: 460
title: "144"
author: ytjohn
date: 10/06/2004
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2004/144
published: true
---
I just got news (well, a day or two ago) that a  former co-worker and good friend fell ill while on vacation.  At the hospital, he was diagnosed with Leukemia.  A day later, he went into a coma.  Within a day or two, he was taken off of life support and died.  The whole thing from feeling sick to death took place within a few days.  Mark Strozzi, you will be missed.
