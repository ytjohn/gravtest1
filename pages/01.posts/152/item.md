---
ID: 465
title: "152"
author: ytjohn
date: 11/07/2004
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2004/152
published: true
---
We have begun.  It was pretty interesting yesterday when everyone was getting staged to go in.  Last night, we took out an 8-building apartment complex and secured a hospital.  Back here on base, all you can hear is the sound of artillery pounding away, all night and all day. There hasn't really been any incoming.  I think the insurgents are being kept pretty busy elsewhere.  The next 14 days will be pretty darn interesting.
