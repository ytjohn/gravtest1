---
ID: 470
title: "160"
author: ytjohn
date: 11/27/2004
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2004/160
published: true
---
I talked about it with some people, but now I have finally launched the site <a href="http://www.iraqagain.com/">Iraq... again</a>.  I am going to put all Iraq-related blog entries and pictures on that site, so start looking there instead of here for a while.  Of course, things that are just my usual musings and findings will naturally go here.
