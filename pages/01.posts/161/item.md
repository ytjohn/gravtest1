---
ID: 478
title: "161"
author: ytjohn
date: 12/13/2004
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2004/161
published: true
---
You know, sometimes it sucks knowing what the right course of action is.  Instant gratification is a bitch to turn down, especially if you don't know how well the "delayed gratification" will turn out.  Why does doing the right thing seem like the boring one?
