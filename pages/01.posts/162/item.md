---
ID: 477
title: "162"
author: ytjohn
date: 12/24/2004
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2004/162
published: true
---
Happy Saturday!  For those of you tuning in back home, then Merry Christmas!  Here in <a href="http://www.userfriendly.org/cartoons/archives/04dec/xuf007435.gif">Iraq</a>, it's business as usual, just another day.  But it is also important to note that for me and my gange, we've past the halfway point and it's all downhill from here.  That is a good cause for celebration.   Cheers.
