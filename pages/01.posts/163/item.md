---
ID: 476
title: "163"
author: ytjohn
date: 01/02/2005
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2005/163
published: true
---
I get a bit worried when someone will come up to me and sincerely thank me for something.  I respond graciously enough with "no problem man", but inside I'm thinking "Who is this person and what do they think I did for them?" Well, I guess it's better than people coming up and cursing me out for things I don't think I did.
