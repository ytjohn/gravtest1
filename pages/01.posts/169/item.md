---
ID: 481
title: "169"
author: ytjohn
date: 09/05/2005
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2005/169
published: true
---
<pre>
i drove to your house
we hopped into the car
i turn the key and we go
we went to the park
we had ice cream and
talked of things of then
and things yet to come
the sun was shining
the kids were rollerblading
music was playing
or so they were saying
is this day a cliche
or is it the truth we seek
where do we go from here
is this something new
or have i been here before
do i take that leap
or play it safe
am i moving forward
or leaving everyone behind
but here we sit at the park
and i guess no more
just another moment
and another moment more
and just a little more
the world can wait
we will remain
</pre>
