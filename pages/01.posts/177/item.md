---
ID: 489
title: "177"
author: ytjohn
date: 11/27/2005
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2005/177
published: true
---
So yeah, it was a good holiday weekend.  Met with family, met with friends (both local and long distance ones) and with the exception of some food poisoning (Which was healed within 4 hours, beat that!), I pretty much had a blast all weekend.  I mean, healing is just amazing when it happens.  Also, I managed to double the list of things that I know one of my long distance friends enjoys doing (bringing the total count to two).<br />
Ok, cheers -- John
