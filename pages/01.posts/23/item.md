---
ID: 333
title: "23"
author: ytjohn
date: 03/29/2002
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2002/23
published: true
---
You can email support of HR256 to: <a HREF="mailto:tom.burch@lrc.state.ky.us"> tom.burch@lrc.state.ky.us</a>, or visit his web page at: <a href="http://www.lrc.state.ky.us/hsedistricts/h030/burch.htm">http://www.lrc.state.ky.us/hsedistricts/h030/burch.htm</a>
