---
ID: 355
title: "40"
author: ytjohn
date: 10/09/2002
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2002/40
published: true
---
According an <a href="http://www.fortune.com/indexw.jhtml?channel=artcol.jhtml&amp;doc_id=209746">article</a>, Gen-X is setup worse than any other generation for a financial crash.  My favorite quote: "I had a college president say to me, 'I don't know how much longer I can pull this off because people will start to ask, Is it worth this much money to be that much smarter?' "<br />
<br />
Credits to slashdot.
