---
ID: 361
title: "45"
author: ytjohn
date: 11/03/2002
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2002/45
published: true
---
Another all nighter.  Why do I put myself through such things?  I feel great the entire night, up until it turns to my time to go into work.  I get to work feeling great, and then I stare at my screen, and the life begins to sap out of me.  Sigh...
