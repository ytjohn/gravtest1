---
ID: 370
title: "63"
author: ytjohn
date: 11/30/2002
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2002/63
published: true
---
Nothing much, just a few links for people that  love links. <br />
A basic philosophical paper on linking laws.
http://www.internet-tips.net/Legal/Linking.htm<br />
A story about a case between TicketMaster and Tickets.com over a "deep linking" dispute.<br />
http://www.wired.com/news/politics/0,1283,35306,00.html
<br /><br />
Oh and a waste management site in the UK. <br />
http://www.brantner.com/en/abfallw/index.jsp
