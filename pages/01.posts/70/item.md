---
ID: 378
title: "70"
author: ytjohn
date: 12/25/2002
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2002/70
published: true
---
Ok, organizing the pictures was easier than I thought it would be.  I did a lot of moving of pictures, so the link in the last update is now invalid, but will at least put you in the proper album.  Also, for family members, I have a hidden album.  Get ahold of me for a username/password to log into it as.  Cheers.
