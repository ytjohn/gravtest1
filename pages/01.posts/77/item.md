---
ID: 387
title: "77"
author: ytjohn
date: 10/31/2003
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2003/77
published: true
---
I'm considering opening up the ability for other people to rant or "blog" on this site.  If anyone is interested in doing so, or would like to see it done, email me.  I'm still mulling it over.
