---
ID: 396
title: "83"
author: ytjohn
date: 11/12/2003
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2003/83
published: true
---
I'm not sure what's more dangerous, eating instant ramen out of a questionably clean beaker, or using glass stirring rods as chopsticks.  -- Credits to <a href="http://www.krakowstudios.com/index.php?strip=krakow20020105.gif">Krakow Studios</a>.
