---
ID: 88
title: "85"
author: ytjohn
date: 11/18/2003
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2003/85
published: true
---
People never seem to understand my signatures or even comment on their cleverness.  Take for example: Wouldn't the sentence "I want to put a hyphen between the words Fish
and And and And and Chips in my Fish-And-Chips sign" have been clearer if
quotation marks had been placed before Fish, and between Fish and and, and
and and And, and And and and, and and and And, and And and and, and and
and Chips, as well as after Chips?
<br /><br />
Shouldn't that at least  get some respect?
