---
ID: 391
title: "86"
author: ytjohn
date: 11/19/2003
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2003/86
published: true
---
Argh!  People are crazy sometimes.  People seem to think that because I'm a relatively smart guy, that I'm somehow above them.  As if being intelligent puts me beyone reproach, beyond question.  Not only is this not true, I'm not even that smart!  I am no genius, I am no scientist.  I have one specialized area that I'm really good at, and people take that to mean that I'm intelligent in many areas.  Do you have any idea what it's like when people simply defer to your opinion because they THINK that you would know better.  Sometimes, I think I know how nobility felt, back in the day.
