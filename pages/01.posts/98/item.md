---
ID: 407
title: "98"
author: ytjohn
date: 01/24/2004
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2004/98
published: true
---
These are amazing times.  I have freed myself from the past, and have already embraced the future.  In other words, I switched jobs and am enjoying my prospects already.
