---
ID: 249
title: 'ANNOUNCE: Game/Movie Night October 20th'
author: ytjohn
date: 09/21/2007
post_excerpt: ""
layout: post
permalink: >
  https://www.yourtech.us/2007/announce-gamemovie-night-october-20th
published: true
---
<pre>My next movie/game night is planned for October 20th, starting at 5pm, roughly.  All are invited.
Let me know if you are planning to make it or not.</pre>
