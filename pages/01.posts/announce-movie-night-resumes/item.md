---
ID: 241
title: 'ANNOUNCE: Movie night resumes'
author: ytjohn
date: 08/27/2007
post_excerpt: ""
layout: post
permalink: >
  https://www.yourtech.us/2007/announce-movie-night-resumes
published: true
---
On Saturday, September 8th we will resume the movie night tradition.Ã‚Â  Somewhere around 5 or 6pm on Saturday, people should congregate at the Hogenmiller Professional Building in Everett, PA.Ã‚Â  There will be movies, food, drinks, and games.Ã‚Â  Please feel free to donate food, drinks, and/or movies to the night's entertainment.
