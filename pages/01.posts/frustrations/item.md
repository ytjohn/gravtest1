---
ID: 151
title: Frustrations
author: ytjohn
date: 05/07/2017
post_excerpt: ""
layout: post
permalink: >
  https://www.yourtech.us/2017/frustrations
published: true
---
This morning I took a meter I was working on outside so I could take it apart and watch my son run around the yard. I planned ahead and took a box to hold the parts. After I had gotten a couple screws out, the wind picked up and blew my box into the yard. I can't find the funny screws in the grass. I should have left them out of the box.
