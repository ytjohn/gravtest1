---
ID: 526
title: 'I&#8217;m on my way'
author: ytjohn
date: 08/09/2017
post_excerpt: ""
layout: post
permalink: >
  https://www.yourtech.us/2017/im-on-my-way
published: true
---
This chart only goes back a bit, but I'm down 27 pounds since April.

<img src="https://static.yourtech.us/yt/uploads/2017/08/Screenshot_2017-08-09-07-27-27.jpg" class="size-full">
