---
ID: 5
title: things are in a state of flux
author: ytjohn
date: 05/06/2017
post_excerpt: ""
layout: post
permalink: >
  https://www.yourtech.us/2017/things-are-in-a-state-of-flux
published: true
---
UPDATE: Content has been re-added, but the published date information is still being corrected.


I am migrating the site to a new server and from mezzanine to wordpress. 

There's always a few thins to work out, and I should be able to restore the content sometime this weekend.
