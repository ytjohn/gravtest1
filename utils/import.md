How to import from wordpress GH sync
------------------------------------

The prior site is stored in [github/ytjohn/yourtech-us](https://github.com/ytjohn/yourtech-us) and uses
the [wp-github-sync](https://wordpress.org/plugins/wp-github-sync/) plugin. 

Example 1: `yourtech-us/_posts/2002-01-08-3-2.md`, with frontmatter like so:

This is an ancient post that lost its title along the way. Have a bunch of these.

```
---
ID: 187
post_title: "3"
author: ytjohn
post_date: 2002-01-08 19:00:00
post_excerpt: ""
layout: post
permalink: https://www.yourtech.us/2002/3-2
published: true
---
```

Old URL: /2002/3-2
slug: 3-2


Steps:

1. mkdir 01.posts/3/
2. cp yourtech-us/_posts/2002-01-08-3-2.md 01.posts/3/item.md
3. s/^post_title: /title: /
4. s/^post_date: /date: /
5. s/^permalink: https:\/\/www.yourtech.us\/....\//slug: /


Example 2: `yourtech-us/_posts/2014-04-29-salt-the-earth.md`, with fronmatter like so:

```
---
ID: 72
post_title: Salt The Earth
author: ytjohn
post_date: 2014-04-29 17:58:11
post_excerpt: ""
layout: post
permalink: >
  https://www.yourtech.us/2014/salt-the-earth
published: true
---
```

Old url: /2014/salt-the-earth
slug: salt-the-earth

1. mkdir 01.posts/salt-the-earth # requires dropping the 2014-04-29- prefix and the .md postfix
2. cp yourtech-us/_posts/2014-04-29-salt-the-earth.md 01.posts/salt-the-earth/item.md
3. s/^post_title: /title: /
4. s/^post_date: /date: /
5. s/^permalink: https:\/\/www.yourtech.us\/....\//slug

