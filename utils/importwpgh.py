#!/usr/bin/env python

import datetime
import os
from os import listdir
from os.path import isfile, join
from shutil import copy
import subprocess

SED = "/usr/bin/sed"

IMPORT_DIR = "/Users/ytjohn/projects/yourtech-us"
TARGET_DIR = "/Users/ytjohn/projects/gravtest1"

OLDPOSTS_DIR = "{}/_posts".format(IMPORT_DIR)
NEWPOSTS_DIR = "{}/pages/01.posts".format(TARGET_DIR)

def copy_file(filename):

    fullpath = "{}/{}".format(OLDPOSTS_DIR, filename)
    published = filename[:10]
    slug = filename[11:][:-3]
    print(fullpath, slug)

    newdir = "{}/{}".format(NEWPOSTS_DIR, slug)
    newfile = "{}/item.md".format(newdir)
    print "  newfile: {}".format(newfile)

    # make directory
    if not os.path.exists(newdir):
        os.mkdir(newdir)

    # copy file, overwriting
    copy(fullpath, newfile)
    return newfile, published

def update_frontmatter(filename, published):

    # post_title: "90"
    # author: ytjohn
    # post_date: 2003 - 12 - 14
    # 19:00:00
    # post_excerpt: ""
    # layout: post
    # permalink: https: // www.yourtech.us / 2003 / 90
    # published: true

    # for some reason, they went with stupid us or eu date instead of an iso.
    # it's really weird because for publish_date, they accept anything strtotime() can support,
    # for the this date they look at the - or the /.

    stupid_us_date = str(datetime.datetime.strptime(
        published, '%Y-%m-%d').strftime('%m/%d/%Y'))

    # there is some cool talk of implementing sed in python
    # here: https://stackoverflow.com/questions/4427542/how-to-do-sed-like-text-replace-with-python
    # but I will just go ahead and use sed
    print("modifying {}".format(filename))
    print(published, stupid_us_date)
    escaped_stupid = stupid_us_date.replace('/', '\/')

    # print("{} -i '' 's/^post_title: /title: /' {}".format(SED, filename))
    subprocess.call("{} -i '' 's/^post_title: /title: /' {}".format(SED, filename), shell=True)
    subprocess.call("{} -i '' 's/^post_date:.*/date: {}/' {}".format(SED, escaped_stupid, filename), shell=True)


def main():
    files = [f for f in listdir(OLDPOSTS_DIR) if isfile(join(OLDPOSTS_DIR, f))]
    for filename in files:
        newfile, published = copy_file(filename)
        update_frontmatter(newfile, published)

if __name__ == '__main__':
    main()
